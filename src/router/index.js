import Vue from 'vue'
import VueRouter from 'vue-router'
import Favorites from '../views/Favorites.vue'
import Home from '../views/Home.vue'
import UserDetail from '../views/UserDetail.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/user/:id',
    name: 'userdetail',
    component: UserDetail,
    props: true
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import()
  },
  {
    path: '/favorites',
    name: 'favorites',
    component: Favorites
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
