import Axios from "axios";


export default function() {
    const apiURL = 'https://randomuser.me/api/?results=100'
    return Axios(apiURL,
        {method: 'GET'},
    )
}

